#include "functionsUva.h"

void jolly(int *arr, int n) {
	int valor;
	//creacion del arreglo que contiene los numeros de 1 a n-1
	int *valores=(int*)malloc(sizeof(int)*n-1);
	for (int h=0; h < n - 1; h++)
		valores[h] = 0;
	//en este arreglo voy insertando los caracteres segun la posicion
	//ejemplo si la resta resultante es 1 le resto a -1 siempre
	//y segun esto inserta el uno en la posicion 0
	for (int i = 0; i < n-1; i++) {
		//abs valor absoluto toma el numero en la pos i y el num en la pos i+1 y los resta
		valor = abs(arr[i] - arr[i + 1]);
		valores[(valor - 1)]=valor;
	}
	int numeros = 1;
	//recorro el arreglo desde la pos 0 y se supone que esta debe 
	//empezar contener el 1 por eso comparo en la pos x si llega a ser diferente 
	//aunque sea una vez durante todo el recorrido del arreglo ya se sabe que no es jolly
	for (int x = 0; x < (n - 1); x++) {
		if (valores[x] != numeros) {
			cout << "Not jolly" << endl;
			return;
		}
		numeros++;
	}
	cout << "Jolly" << endl;

}

void hartals(int N, int P, int *party_h) {
	int days[3650];
	
	/* Se llena el arreglo con ceros hasta 
	el dia en que se hace la prueba */
	for (int i = 0; i <= N; i++) {
		days[i] = 0;
	}

	/* Recorro la lista de hartals por partido
	para marcar el hartals en la matriz dias */
	for (int party = 0; party < P; party++) {
		int day = party_h[party];
		/* Se itera cada dia que el partido espera hacer un hartal
		hasta el dia en que se hace la prueba */
		while (day <= N) {
			/* El modulo con siete me permite saber la posicion
			de un dia desde el uno al siete, repectivamente
			de domingo a sabado. Si el modulo es 6,
			corresponde a un viernes. Si es cero correponde 
			a un sabado. Cualquier dia diferente a estos
			marca un hartal epara ese dia en el arreglo days */
			if (day % 7 != 6 && day % 7 != 0) {
				days[day]++;
			}
			day += party_h[party];
		}
	}

	/* Recorro el arreglo de dias hasta el dia de la prueba,
	si hay uno o mas hartas por dia, sumo uno a los dias en
	que se hizo hartals */
	int hartals = 0;
	for (int i = 1; i <= N; i++) {
		if (days[i] != 0) {
			hartals++;
		}
	}

	cout << hartals << endl;
}

void articlePay() {
	int n, k, m;
	//el tama�o del arreglo en ASCII es 256 
	int caracteres[256] = { 0 };
	cout << "enter n the amount of test to analized" << endl;
	cin >> n;
	for (int i = 0; i<n; i++) {

		cout << "enter the amount of chars to paid" << endl;
		cin >> k;
		//ciclo para poner los valores de las letras
		for (int z = 0; z < k; z++) {
			int precio;
			//esto para poder tener disponible mas espacio 
			//para los chars un unsigned char me da de 0 a 255
			unsigned char caract;
			cout << "enter the char and the price" << endl;
			cin >> caract >> precio;
			//int caract es para castear el char a su numero segun ASCII
			caracteres[(int)caract] = precio;
		}
		cout << "enter m for the amount line breaks of the program" << endl;
		cin >> m;
		//ciclo 
		//total que me cuenta el valor del texto
		int total = 0;
		//x va hasta m el cual m es la cantidad de saltos de linea que tiene el escrito
		for (int x = 0; x <= m; x++) {
			//unsigned para mayor tama�o y poder tener el desde 0 hasta los 255 del codigo ASCII
			unsigned char letters;
			//cada vez que va escribiendo la letra el comprueba si es un salto de linea termina ese 
			//while y si no lo es entra en el while y revisa si el caracter que la persona 
			//escribio esta entre la lista de caracteres, introducidos 
			while ((letters = cin.get())!='\n') {
				total += caracteres[(int)letters];
			}
		}
		printf("%.2lf$\n", total / 100.0);
	}
	
}

/*
Funci�n imprimir_arreglos:
Imprime la combinacion ordenada de dos arreglos ordenados.

pre: A = {a_0, ..., a_(n-1)}, B = {b_0, ..., b_(n-1)}, con 1 <= n, m <= 10000
Ambos arreglos contienen enteros ordenados de forma ascendente.

post:

*/
void imprimir_arreglos(int* A, int* B, int n, int m) {
	int i = 0, j = 0;

	while (i < n && j < m) {

		if (A[i] < B[j])
			printf("%d ", A[i++]);
		else
			printf("%d ", B[j++]);
	}

	while (i < n)
		printf("%d ", A[i++]);

	while (j < m)
		printf("%d ", B[j++]);

	printf("\n");
}

void light_transparencies() {
	int test, nl, flag = 0;

	printf("Number of cases: ");
	scanf_s("%d", &test);
	while (test--) {
		if ( flag == 0 ) {
			flag = 1;
		} else {
			printf("\n");
		}
		printf("Number of segments: ");
		scanf_s("%d", &nl);
		int number_intervals;
		double** segments = new double*[nl];
		for (int i = 0; i < nl; i++) {
			segments[i] = new double[5];
		}
		number_intervals = 2 * nl;
		double* inv = new double[number_intervals];
		double* seg = new double[number_intervals];
		double* a_coef = new double[number_intervals];
		int posTem = 0;
		for (int i = 0; i < nl; i++) {
			double x1, x2, y1, y2, r;
			printf("Coords and coeficent of segments: ");
			scanf_s("%lf %lf %lf %lf %lf", &x1, &y1, &x2, &y2, &r);

			segments[i][0] = x1;
			segments[i][1] = y1;
			segments[i][2] = x2;
			segments[i][3] = y2;
			segments[i][4] = r;
			/* Organiza las posiciones por pares segun el inicio y fin
			del segmento */
			if (x1 < x2) {
				seg[posTem] = x1;
				inv[posTem] = x1;
				seg[posTem + 1] = x2;
				inv[posTem + 1] = x2;
				a_coef[posTem] = r;
				a_coef[posTem + 1] = r;
			}
			else {
				seg[posTem] = x2;
				inv[posTem] = x2;
				seg[posTem + 1] = x1;
				inv[posTem + 1] = x1;
				a_coef[posTem] = r;
				a_coef[posTem + 1] = r;
			}
			posTem += 2;
		}

		/* Al organizar el arreglo de segmentos obtengo el arreglo
		de intervalos con sus respectivas intersecciones */
		sort(inv, inv + number_intervals);

		printf("%d \n", number_intervals + 1);

		/* Recorre el arreglo de intervalos */
		for (int i = 0; i < number_intervals - 1; i++) {
			double light = 1.0;
			if (i == 0) {
				printf("-inf %.3f 1.000\n", inv[i]);
			}
			/* Si el intervalo, incluido su inicio y fin, esta
			incluido en el segmento, se cruza la luz por el respectivo
			coeficiente del segmento y ese intervalo tendra el coeficiente
			en donde este incluido para los segmentos*/
			for (int k = 0; k < number_intervals - 1; k+=2) {
				if (inv[i] >= seg[k] && inv[i + 1] <= seg[k + 1]) {
					light *= a_coef[k];
				}
			}
			printf("%.3f %.3f %.3f\n", inv[i], inv[i + 1], light);
			if (i + 1 == number_intervals - 1) {
				printf("%.3f +inf 1.000\n", inv[i + 1]);
			}
		}
	}
}