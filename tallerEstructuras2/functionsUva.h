#include <string>
#include <algorithm>
#include <iostream>
#include <stdlib.h>
#include <stdio.h>
using namespace std;

#ifndef FUNCTIONSUVA_H
#define FUNCTIONSUVA_H
/*
	jolly 
	una sucessionde numeros es jolly si dado un arreglo el valor 
	absoluto de las restas de los numeros sucesivos da los numeros desde 
	1 hasta n-1
	@param arreglo de enteros
	@param tama�o 
	*/
void jolly(int *, int );

/*
	hartals
	Purpose: Calcular los dias en que un grupo de partidos no trabajan
	@param int: Numero de dias a hacer la prueba
	@param int: Numero de partidos
	@param int*: Arreglo con numero de dias que los partidos hacen hartals
	@return void
*/
void hartals(int, int, int *);
void articlePay();

/*
	light_transparencies
	Purpose: Imprimir intervalos segmentados segun un coeficiente de refraccion donde
			 atraviesa luz	
	@input int: Numero de pruebas
	@input int: Numero de segmentos por donde pasa la luz
		@input double: Inicio del segmento en x
		@input double: Inicio del segmento en y
		@input double: Fin del segmento en x
		@input double: Fin del segmento en x
		@input double: Coefiente de refraccion del segmentos
	@return void
*/
void light_transparencies();
void imprimir_arreglos(int* A, int* B, int n, int m);

#endif FUNCTIONSUVA_H

